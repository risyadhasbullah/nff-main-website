import os
import environ

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
#BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
root = environ.Path(__file__)-2 #Go to the root folder /nff_main
env = environ.Env(DEBUG=(bool, False),)
BASE_DIR = root()
environ.Env.read_env()


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('SECRET_KEY', default="randomstring")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env('DEBUG', default=True)

ALLOWED_HOSTS = ['thenff.herokuapp.com' , '127.0.0.1', 'the12thnff.com', 'www.the12thnff.com']

# Application definition

INSTALLED_APPS = [
    'nff.apps.NffConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'gdstorage',
]

GOOGLE_DRIVE_STORAGE_JSON_KEY_FILE = BASE_DIR + '/nff_main/My Project-a7c73222bc6e.json'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'nff_main.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'nff_main.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR + 'db.sqlite3',
    }
}
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': env('DB_NAME', default='d37bjisbh4a78m'),
#         'USER': env('DB_USER', default='vpeuinrqrbhaul'),
#         'PASSWORD': env('DB_PASSWORD', default='783e2afbc38be5e4d0ee4912c155dcb92fc1fbeb578850888f80fcdd582819ad'),
#         'HOST': env('DB_HOST', default='ec2-23-21-158-253.compute-1.amazonaws.com'),
#         'PORT': env('DB_PORT', default='5432'),
#     }
# }


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


DEFAULT_FROM_EMAIL = 'admin@the12thnff.com'
SERVER_MAIL = 'admin@the12thnff.com'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_SSL = True
EMAIL_PORT = 465
EMAIL_HOST = 'smtp.zoho.com'
#EMAIL_PORT = 587
#EMAIL_USE_TLS = True
EMAIL_HOST_USER = 'admin@the12thnff.com'
EMAIL_HOST_PASSWORD = 'dcd-Yzz-o3d-ePW'


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = "/app/nff/static/"
MEDIA_ROOT = '/home/fokkeri2/public_html/uploads'
MEDIA_URL = '/uploads/'
FILE_UPLOAD_PERMISSIONS = 0o644
