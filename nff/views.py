# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView
from django.views import generic
from django.http import Http404
from .forms import ConfirmForm, UploadForm, RegisterForm
from django.core.mail import send_mail
from django.template.loader import get_template
from .models import Registration

# Create your views here.
def index(request):
    return render(request, 'nff/index.html')

def about(request):
    return render(request, 'nff/about.html')

def competition(request):
    return render(request, 'nff/competition.html')

def success(request):
    return render(request, 'nff/success-padus.html')

def application(request):
    return render(request, 'nff/application_form.html')

def gallery_tari(request):
    return render(request, 'nff/gallery-tari.html')

def gallery_padus(request):
    return render(request, 'nff/gallery-padus.html')

def poster(request):
    return render(request, 'nff/poster.html')

def register(request):
    return render(request, 'nff/register-closed.html')


class Register(generic.View):
    form_class = RegisterForm
    template_name = 'nff/registration_form.html'

    #display blank form
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form':form})

    #process form data
    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():
            validform = form.save(commit=False)

            #normalized data
            email_contact_person = form.cleaned_data['email_contact_person']
            kategori = form.cleaned_data['kategori']
            if "Padus" in str(kategori):
                send_mail(
                    'Terima kasih telah mendaftar NFF!',
                    '''
                    Terima kasih sudah mendaftar dalam kompetisi Paduan Suara The 12th NFF.
                    Untuk pembayaran, dapat transfer ke rekening:
                    BNI a/n Klara Amanda Aninditya
                    0585465368

                    Dengan jumlah:
                    Kategori A - Rp400.000
                    Kategori B - Rp450.000

                    Ketika sudah melakukan pembayaran dan melakukan konfirmasi pada kami, tim anda secara resmi sudah menjadi peserta The 12th NFF.

                    Kami tunggu pembayarannya karena kuota terbatas.

                    Terima kasih.
                     ''',
                    'admin@the12thnff.com',
                    [email_contact_person],
                    fail_silently=False,
                    html_message=
                    '''
                    <html>
                    <head>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
                    </head>
                    <body>
                    <div style="text-align:center;">
                    <div class="image-container">
                        <img style="width: 7em;" src="https://the12thnff.com/static/nff/images/icon.png">
                    </div>
                    <h1 style="color: #d5b135;">Terima kasih sudah mendaftar dalam kompetisi Paduan Suara The 12th NFF.</h1>
                    <p>Untuk pembayaran, dapat transfer ke rekening:<br />
                    BNI a/n Klara Amanda Aninditya<br />
                    0585465368<br /><br />

                    Dengan jumlah:<br />
                    Kategori A - Rp400.000<br />
                    Kategori B - Rp450.000<br /><br />

                    Ketika sudah melakukan pembayaran dan melakukan konfirmasi pada kami, tim anda secara resmi sudah menjadi peserta The 12th NFF.<br /><br />

                    Kami tunggu pembayarannya karena kuota terbatas.<br /><br />

                    Terima kasih.</p>
                    </div>
                    </body>
                    </html>
                     '''
                )
                validform.save()
                return render(request, 'nff/success-padus.html')
            else:
                send_mail(
                    'Terima kasih telah mendaftar NFF!',
                    '''
                    Terima kasih sudah mendaftar dalam kompetisi Tari Tradisional The 12th NFF.
                    Untuk pembayaran, dapat transfer ke rekening:
                    BNI a/n Klara Amanda Aninditya
                    0585465368

                    Dengan jumlah:
                    Kategori A - Rp300.000
                    Kategori B - Rp325.000
                    Kategori C - Rp350.000

                    Ketika sudah melakukan pembayaran dan melakukan konfirmasi pada kami, tim anda secara resmi sudah menjadi peserta The 12th NFF.

                    Kami tunggu pembayarannya karena kuota terbatas.

                    Terima kasih.
                     ''',
                    'admin@the12thnff.com',
                    [email_contact_person],
                    fail_silently=False,
                    html_message=
                    '''
                    <html>
                    <head>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
                    </head>
                    <body>
                    <div style="text-align:center;">
                    <div class="image-container">
                        <img style="width: 7em;" src="https://the12thnff.com/static/nff/images/icon.png">
                    </div>
                    <h1 style="color: #d5b135;">Terima kasih sudah mendaftar dalam kompetisi Tari Tradisional The 12th NFF.</h1>
                    <p>Untuk pembayaran, dapat transfer ke rekening:<br />
                    BNI a/n Klara Amanda Aninditya<br />
                    0585465368<br /><br />

                    Dengan jumlah:<br />
                    Kategori A - Rp300.000<br />
                    Kategori B - Rp325.000<br />
                    Kategori C - Rp350.000<br /><br />

                    Ketika sudah melakukan pembayaran dan melakukan konfirmasi pada kami, tim anda secara resmi sudah menjadi peserta The 12th NFF.<br /><br />

                    Kami tunggu pembayarannya karena kuota terbatas.<br /><br />

                    Terima kasih.</p>
                    </div>
                    </body>
                    </html>
                     '''
                )
                validform.save()
                return render(request, 'nff/success-tari.html')

        return render(request, self.template_name, {'form':form})


class Confirm(generic.View):
    form_class = ConfirmForm
    template_name = 'nff/confirmation_form.html'

    #display blank form
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form':form})

    #process form data
    def post(self, request):
        form = self.form_class(request.POST, request.FILES)

        if form.is_valid():
            validform = form.save(commit=False)

            #normalized data
            nama_tim = form.cleaned_data['nama_tim']
            tim = Registration.objects.get(nama_tim=nama_tim)
            email_contact_person = tim.email_contact_person
            kategori = form.cleaned_data['kategori']
            if "Padus" in str(kategori):
                send_mail(
                    'Terima kasih telah melakukan pembayaran',
                    '''
                    Terima kasih sudah melakukan pembayaran.

                    Selamat! Anda telah resmi bergabung dalam The 12th NFF.
                    Untuk selanjutnya, silakan download dan isi application form.
                    Batas upload form sampai dengan tanggal 8 Januari 2018.
                    Terima kasih.

                    https://the12thnff.com/static/nff/download/APPLICATION%20FORM%20PADUS.docx
                     ''',
                    'admin@the12thnff.com',
                    [email_contact_person],
                    fail_silently=False,
                    html_message=
                    '''
                    <html>
                    <head>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
                    </head>
                    <body>
                    <div style="text-align:center;">
                    <div class="image-container">
                        <img style="width: 7em;" src="https://the12thnff.com/static/nff/images/icon.png">
                    </div>
                    <h1 style="color: #d5b135;">Terima kasih sudah melakukan pembayaran.</h1>
                    <p>
                      Selamat! Anda telah resmi bergabung dalam The 12th NFF.<br /><br />
                      Untuk selanjutnya, silakan download dan isi application form.<br /><br />
                      Batas upload form sampai dengan tanggal 8 Januari 2018.<br /><br />
                      Terima kasih.
                    </p>
                    <a href="https://the12thnff.com/static/nff/download/APPLICATION%20FORM%20PADUS.docx" download="12th NFF - APPLICATION FORM PADUS.docx">12th NFF - APPLICATION FORM PADUS</a><br />
                    </div>
                    </body>
                    </html>
                     '''
                )
                validform.save()
                return render(request, 'nff/success-confirm-padus.html')
            else:
                send_mail(
                    'Terima kasih telah melakukan pembayaran',
                    '''
                    Terima kasih sudah melakukan pembayaran.

                    Selamat! Anda telah resmi bergabung dalam The 12th NFF.
                    Untuk selanjutnya, silakan download dan isi application form.
                    Batas upload form sampai dengan tanggal 8 Januari 2018.
                    Terima kasih.

                    https://the12thnff.com/static/nff/download/APPLICATION%20FORM%20TARI.docx
                     ''',
                    'admin@the12thnff.com',
                    [email_contact_person],
                    fail_silently=False,
                    html_message=
                    '''
                    <html>
                    <head>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
                    </head>
                    <body>
                    <div style="text-align:center;">
                    <div class="image-container">
                        <img style="width: 7em;" src="https://the12thnff.com/static/nff/images/icon.png">
                    </div>
                    <h1 style="color: #d5b135;">Terima kasih sudah melakukan pembayaran.</h1>
                    <p>
                      Selamat! Anda telah resmi bergabung dalam The 12th NFF.<br /><br />
                      Untuk selanjutnya, silakan download dan isi application form.<br /><br />
                      Batas upload form sampai dengan tanggal 8 Januari 2018.<br /><br />
                      Terima kasih.
                    </p>
                    <a href="https://the12thnff.com/static/nff/download/APPLICATION%20FORM%20TARI.docx" download="12th NFF - APPLICATION FORM TARI.docx">12th NFF - APPLICATION FORM TARI</a><br />
                    </div>
                    </body>
                    </html>
                     '''
                )
                validform.save()
                return render(request, 'nff/success-confirm-tari.html')

        return render(request, self.template_name, {'form':form})


class Form(generic.View):
    form_class = UploadForm
    template_name = 'nff/upload_form.html'

    #display blank form
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form':form})

    #process form data
    def post(self, request):
        form = self.form_class(request.POST, request.FILES)

        if form.is_valid():
            validform = form.save(commit=False)

            #normalized data
            email_contact_person = form.cleaned_data['nama_tim']
            validform.save()
            return redirect('nff:success')


        return render(request, self.template_name, {'form':form})
