# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import Category, Registration, Confirmation, Form_upload

# Register your models here.
class RegisAdmin(admin.ModelAdmin):
    model = Registration
    list_display = ['nama_tim', 'asal_tim', 'kategori', 'nama_contact_person', 'nomor_contact_person' ,'email_contact_person' ]


class ConfirmAdmin(admin.ModelAdmin):
    model = Confirmation
    list_display = ['nama_tim', 'asal', 'kategori', 'nama_rekening_pengirim', 'tanggal_transfer', 'contoh' ]
    readonly_fields = ('asal',)

    def asal(self, obj):
        return obj.asal()

    def contoh(self, obj):
        return obj.contoh()


class CategoryAdmin(admin.ModelAdmin):
    model = Category
    list_display = ['id', 'category_type']

admin.site.register(Category, CategoryAdmin)
admin.site.register(Confirmation, ConfirmAdmin)
admin.site.register(Form_upload)
admin.site.register(Registration, RegisAdmin)
admin.site.site_title = 'NFF Admin'
admin.site.site_header = 'NFF Admin'
