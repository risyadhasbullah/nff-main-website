# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-24 03:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nff', '0017_auto_20171024_0334'),
    ]

    operations = [
        migrations.AlterField(
            model_name='confirmation',
            name='tanggal_transfer',
            field=models.DateField(),
        ),
    ]
