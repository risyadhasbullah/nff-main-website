# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse
from gdstorage.storage import GoogleDriveStorage
import os
from django.db.models import Q

# Create your models here.
gd_storage = GoogleDriveStorage()
def bukti_bayar_file_name(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s_%s.%s" % (instance.nama_tim, 'bukti_bayar', ext)
    return os.path.join('bukti_bayar', filename)

def forms_file_name(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s_%s.%s" % (instance.nama_tim, 'forms', ext)
    return os.path.join('forms', filename)

class Category(models.Model):
    class Meta:
        verbose_name_plural = "categories"

    category_type = models.CharField(max_length=31)

    def __str__(self):
        return self.category_type

class Registration(models.Model):
    nama_tim = models.CharField(max_length=30, unique=True)
    asal_tim = models.CharField(max_length=30)
    kategori = models.ForeignKey(Category, on_delete=models.CASCADE, limit_choices_to=Q(id=1) | Q(id=2))
    nama_contact_person = models.CharField(max_length=30)
    nomor_contact_person = models.CharField(max_length=30)
    email_contact_person = models.EmailField(max_length=50, unique=True)

    def get_absolute_url(self):
        return reverse('nff:index')

    def __str__(self):
        return self.nama_tim

class Confirmation(models.Model):
    from django.utils import timezone
    nama_tim = models.ForeignKey(Registration, to_field='nama_tim')
    kategori = models.ForeignKey(Category, on_delete=models.CASCADE)
    nama_rekening_pengirim = models.CharField(max_length=30)
    tanggal_transfer = models.DateField(default=timezone.now())
    bukti = models.ImageField(upload_to=bukti_bayar_file_name)

    def __str__(self):
        return '%s' % (self.nama_tim)

    def asal(self):
        return 'Asal {:10}'.format(self.nama_tim.asal_tim)

    def contoh(self):
        return os.path.basename(self.bukti.name)

    @property
    def filename(self):
       return os.path.basename(self.bukti.name)

class Form_upload(models.Model):
    nama_tim = models.ForeignKey(Registration, to_field='nama_tim')
    form = models.FileField(upload_to=forms_file_name)

    def __str__(self):
        return str(self.nama_tim)
