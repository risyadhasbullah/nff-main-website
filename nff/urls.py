from django.conf.urls import url
from django.contrib import admin
from . import views

app_name="nff"

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^tentang/$', views.about, name='about'),
    url(r'^registrasi/$', views.register, name='registration'),
    url(r'^konfirmasi/$', views.Confirm.as_view(), name='confirmation'),
    url(r'^unggah_formulir/$', views.Form.as_view(), name='form_upload'),
    url(r'^success/$', views.success, name='success'),
    url(r'^formulir/$', views.application, name='application'),
    url(r'^home/$', views.index, name='home'),
    url(r'^kompetisi/$', views.competition, name='competition'),
    url(r'^galeri_tari/$', views.gallery_tari, name='gallery_tari'),
    url(r'^galeri_padus/$', views.gallery_padus, name='gallery_padus'),
    url(r'^poster/$', views.poster, name='poster'),
]
