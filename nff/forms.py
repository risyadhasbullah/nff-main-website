from django import forms
from .models import Confirmation, Form_upload, Registration

class ConfirmForm(forms.ModelForm):

    class Meta:
        model = Confirmation
        fields = ['nama_tim', 'kategori', 'nama_rekening_pengirim', 'tanggal_transfer', 'bukti']
        widgets = {
            'nama_tim': forms.TextInput(),
            'tanggal_transfer': forms.SelectDateWidget()
        }
        error_messages = {
            'nama_tim':{
                'invalid_choice': "We don't have that team name registered. Please check again, the name is case sensitive",
                }
            }

class UploadForm(forms.ModelForm):

    class Meta:
        model = Form_upload
        fields = ['nama_tim', 'form']
        widgets = {
            'nama_tim': forms.TextInput()
        }
        error_messages = {
            'nama_tim':{
                'invalid_choice': "We don't have that team name registered. Please check again, the name is case sensitive",
                }
            }


class RegisterForm(forms.ModelForm):
    class Meta:
        model = Registration
        fields = ['nama_tim','asal_tim', 'kategori', 'nama_contact_person','nomor_contact_person', 'email_contact_person']
